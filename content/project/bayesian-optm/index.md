---
title:  Dynamic modulation of brain states using brain stimulation and neuroadaptive Bayesian optimisation
summary: Bayesian optimisation applied to brain stimulation.
tags:
- optimisation
- brain stimulation
date: "2021-02-01T00:00:00Z"

# Optional external URL for project (replaces project detail page).
external_link: ""

image:
  caption: 
  focal_point: Smart

#links:
#- icon: 
#  icon_pack: 
#  name: 
#  url: ""
#url_code: ""
#url_pdf: ""
#url_slides: ""
#url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: example
---

In order to support cognitive functions, the brain must coordinate the interactions among large-scale networks that cooperate and compete to allow for efficient transitions between brain states. Understanding how these operate, giving rise to different behaviours is one of the greatest challenges facing modern neuroscience. 

The overarching aim of this project is to develop a framework capable of shaping the interactions between brain networks. In order to do this, we will combine transcranial alternating current stimulation (tACS) with a novel approach, neuroadaptive Bayesian optimization. 

TACS is a promising tool to modulate brain function. The oscillatory electrical activity imposed by tACS has been shown to result in neural modulations that spread along brain networks. However, there are two main limitations to the application of tACS to modulate brain function: 1) the brain networks targeted by stimulation cannot be verified in the absence of brain imaging; 2) the stimulation parameters vary across individuals, due to a multitude of variables, such as age, sex and genetic factors. 

Thus, identifying the optimal stimulation protocol that drives a particular brain state in a given individual is like finding a needle in a haystack. To address these fundamental challenges, we propose to use neuroadaptive Bayesian optimization, which uses a close-loop search combining real-time fMRI with machine learning. This approach conducts an automatic and intelligent search across the multitude of tACS parameters in order to identify those that optimally elicit a particular target state. This framework has translational potential, as several psychiatric and neurological conditions are associated with impaired function of large-scale brain networks. The results of this project can lead to the development of therapeutic interventions that harness the potential of brain stimulation. 
