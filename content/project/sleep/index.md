---
title: Neuromodulation of wake and sleep rhythms
summary: Closed-loop brain stimulation of brain states.
tags:
- sleep
- brain stimulation
date: "2021-02-01T00:00:00Z"

# Optional external URL for project (replaces project detail page).
external_link: ""

image:
  caption: 
  focal_point: Smart

#links:
#- icon: 
#  icon_pack: 
#  name: 
#  url: ""
#url_code: ""
#url_pdf: ""
#url_slides: ""
#url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
# slides: example
---

Sleep is both mysterious in its functions and distinctive in its characteristics - [Henry's]({{< ref "../../content/authors/henry/_index.md" >}}) research aims to draw a line between the two via targeted modulation of ongoing brain activity in humans. The over-arching goal here is to optimally perturb or enhance the EEG rhythms which distinguish different states of vigilance. If the composite electrical oscillations of sleep may be selectively modulated, their respective roles in brain maintenance may be more readily elucidated.
