---
title: Developmental Coordination Disorder - Top-down vs Bottom-up
summary: research in DCD in adulthood
tags:
- neurodevelopment

date: "2021-02-01T00:00:00Z"

# Optional external URL for project (replaces project detail page).
external_link: ""

image:
  caption: 
  focal_point: Smart

#links:
#- icon: 
#  icon_pack: 
#  name: 
#  url: ""
#url_code: ""
#url_pdf: ""
#url_slides: ""
#url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
# slides: example
---

Developmental Coordination Disorder (DCD/Dyspraxia) is a neurodevelopmental disorder characterised by fine and gross motor impairments, in which executive functioning difficulties have been reported. Inhibitory control is a core executive function required for efficient goal directed-motor operations. Previous studies of motor inhibition in DCD indicate impaired action restraint abilities. However, little evidence is available to ascertain whether this impairment extends to action cancellation processes and whether slower bottom-up information processing plays a role in motor inhibition control in DCD. 

We performed a study to investigate this question using two complementary paradigms that can disentangle information processing speed from executive motor inhibition: The Choice Reaction and Stop Signal Task paradigms. By performing Bayesian modelling we show that while participants with DCD had difficulty initiating a Go response, there was no evidence for impairments in Stop actions. 

The current findings indicate that there is not an executive motor inhibition deficit in DCD and suggest that the results of previous research may be better explained by inefficient information processing. 
